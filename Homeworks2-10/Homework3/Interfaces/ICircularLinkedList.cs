﻿using System.Collections.Generic;

namespace Homework3.Interfaces
{
    interface ICircularLinkedList<T> : IEnumerable<T>
    {
        int Count { get; }

        bool IsEmpty { get; }

        void Add(T data);

        bool Remove(T data);

        bool Contains(T data);

        void Clear();
    }
}

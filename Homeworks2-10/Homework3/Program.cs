﻿using Homework3.Interfaces;
using System;

namespace Homework3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Let's test linked list");
            var list = new CircularLinkedList<string>("value", "one more value", "last one value");
            PrintListStats(list);
            list.Add("Yo");
            list.Add("TEST");
            list.Remove("value");
            PrintListStats(list);
            Console.ReadKey();
        }

        private static void PrintListStats(ICircularLinkedList<string> list)
        {
            Console.WriteLine($"Amount of records: {list.Count}");
            Console.WriteLine("Records:");
            foreach (var value in list)
            {
                Console.WriteLine(value);
            }
        }
    }
}

﻿using Homework3.Interfaces;
using System.Collections;
using System.Collections.Generic;

namespace Homework3
{
    public class CircularLinkedList<T> : ICircularLinkedList<T>
    {
        private Node<T> _head;
        private Node<T> _tail;

        public int Count { get; private set; }
        public bool IsEmpty => Count == 0;

        public CircularLinkedList(params T[] initialData)
        {
            foreach (var data in initialData)
            {
                Add(data);
            }
        }

        public void Add(T data)
        {
            var node = new Node<T>(data);

            if (_head == null)
            {
                _head = node;
                _tail = node;
                _tail.Next = _head;
            }
            else
            {
                node.Next = _head;
                _tail.Next = node;
                _tail = node;
            }
            Count++;
        }

        public bool Remove(T data)
        {
            var current = _head;
            Node<T> previous = null;

            if (IsEmpty) 
                return false;

            do
            {
                if (current.Data.Equals(data))
                {
                    if (previous != null)
                    {
                        previous.Next = current.Next;

                        if (current == _tail)
                            _tail = previous;
                    }
                    else
                    {

                        if (Count == 1)
                        {
                            _head = _tail = null;
                        }
                        else
                        {
                            _head = current.Next;
                            _tail.Next = current.Next;
                        }
                    }
                    Count--;
                    return true;
                }

                previous = current;
                current = current.Next;
            } while (current != _head);

            return false;
        }

        public bool Contains(T data)
        {
            var current = _head;
            if (current == null) 
                return false;
            do
            {
                if (current.Data.Equals(data))
                    return true;
                current = current.Next;
            }
            while (current != _head);

            return false;
        }

        public void Clear()
        {
            _head = null;
            _tail = null;
            Count = 0;
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            var current = _head;
            do
            {
                if (current != null)
                {
                    yield return current.Data;
                    current = current.Next;
                }
            }
            while (current != _head);
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return ((IEnumerable<T>)this).GetEnumerator();
        }
    }
}

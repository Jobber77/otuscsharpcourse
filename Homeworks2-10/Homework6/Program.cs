﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Homework6.Extensions;
using Homework6.Models;

namespace Homework6
{
    class Program
    {
        private static User _currentUser = null;
        // assume these arrays get filled from somewhere.
        private static List<User> _usersList = new List<User>()
        {
            new User
            {
                Id = 1,
                Name = "Elwis",
                Login = "elwis.p",
                Password = "123"
            },
            new User
            {
                Id = 2,
                Name = "Harry",
                Login = "harry.p",
                Password = "321"
            }
        };
        private static List<Account> _accountsList = new List<Account>()
        {
            new Account
            {
                Id = 1,
                UserId = 1,
                AccountBalance = 100500,
                OpeningDate = DateTimeOffset.Now
            },
            new Account
            {
                Id = 2,
                UserId = 1,
                AccountBalance = 10,
                OpeningDate = DateTimeOffset.Now
            },
            new Account
            {
                Id = 3,
                UserId = 2,
                AccountBalance = 100,
                OpeningDate = DateTimeOffset.Now
            }
        };
        private static List<History> _historyList = new List<History>()
        {
            new History
            {
                Id = 1,
                OperationAmount = 20,
                OperationDate = DateTimeOffset.Now,
                AccountId = 1,
                OperationType = AccountOperationType.Add
            },
            new History
            {
                Id = 2,
                OperationAmount = 200,
                OperationDate = DateTimeOffset.Now,
                AccountId = 1,
                OperationType = AccountOperationType.Add
            },
            new History
            {
                Id = 3,
                OperationAmount = 320,
                OperationDate = DateTimeOffset.Now,
                AccountId = 1,
                OperationType = AccountOperationType.Add
            },
            new History
            {
                Id = 4,
                OperationAmount = 1110,
                OperationDate = DateTimeOffset.Now,
                AccountId = 2,
                OperationType = AccountOperationType.Subtract
            }
        };

        static void Main(string[] args)
        {
            LoginUser();

            string command = default;
            while (!string.Equals(command, "exit", StringComparison.InvariantCultureIgnoreCase))
            {
                ProcessCommand(command);
                command = Console.ReadLine();
            }
            Console.Beep();
        }

        private static void LoginUser()
        {
            while (_currentUser == null)
            {
                Console.WriteLine("Введите логин юзера:");
                var login = Console.ReadLine();
                Console.WriteLine("Введите пароль юзера:");
                var password = Console.ReadLine();

                _currentUser = _usersList.FirstOrDefault(item =>
                    string.Equals(item.Login, login, StringComparison.InvariantCultureIgnoreCase) && item.Password == password);

                Console.WriteLine(_currentUser == null
                    ? "Такого пользователя не существует, или пароль не верен. Попробуйте снова."
                    : $"Привет, {_currentUser.Name}");
            }
        }

        private static void ProcessCommand(string command)
        {
            var text = String.Empty;
            switch (command)
            {
                case "help":
                    text = GetHelpText();
                    break;
                case "user":
                    text = _currentUser.ToString();
                    break;
                case "accounts":
                    text = GetAccountsText();
                    break;
                case "history":
                    text = GetAccountsHistoryText();
                    break;
                case "add-operations":
                    text = GetAddOperationsText();
                    break;
                case "who-has-money":
                    text = GetUserWithAccountMoneyText();
                    break;
                default:
                    text = "Пожалуйста, введите команду. Чтобы увидеть список доступных команд, введите \"help\"";
                    break;

            }
            Console.WriteLine(text);
        }

        private static string GetHelpText() => $"available commands: {Environment.NewLine} \"user\" - показать инфо по текущему пользователю" +
                   $"{Environment.NewLine} \"accounts\" - показать аккаунты текущего пользователя" +
                   $"{Environment.NewLine} \"history\" - показать историю операций по аккаунтам текущего пользователя" +
                   $"{Environment.NewLine} \"add-operations\" - показать все операции пополнения по всем пользователям" +
                   $"{Environment.NewLine} \"who-has-money\" - показать всех пользователей, у которых денег больше, чем вы укажете";

        private static string GetAccountsText()
        {
            var accounts = _accountsList.Where(accont => accont.UserId == _currentUser.Id);
            var sb = new StringBuilder();
            accounts.ForEach(account => sb.Append($"{account.ToString()}{Environment.NewLine}"));
            return sb.ToString();
        }

        private static string GetAccountsHistoryText()
        {
            var accounts = _accountsList.Where(accont => accont.UserId == _currentUser.Id);
            var sb = new StringBuilder();
            foreach (var account in accounts)
            {
                sb.Append($"{account.ToString()}{Environment.NewLine}History for this account:{Environment.NewLine}");
                var accountHistory = _historyList.Where(record => record.AccountId == account.Id);
                accountHistory.ForEach(record => sb.Append($"{record.ToString()}{Environment.NewLine}"));
            }
            return sb.ToString();
        }

        private static string GetAddOperationsText()
        {
            var addOpetations = _historyList
                .Where(record => record.OperationType == AccountOperationType.Add)
                .Join(_accountsList, history => history.AccountId, account => account.Id,
                        (history, account) => $"{account.ToString()}: {history.ToString()}");

            var sb = new StringBuilder();
            addOpetations.ForEach(row => sb.Append($"{row}{Environment.NewLine}"));
            return sb.ToString();
        }

        private static string GetUserWithAccountMoneyText()
        {
            Console.WriteLine("Введите количество денег:");
            var money = Convert.ToDecimal(Console.ReadLine());
            var users = _accountsList.Where(account => account.AccountBalance >= money)
                .Join(_usersList, account => account.UserId, user => user.Id, (account, user) => user)
                .Distinct();

            var sb = new StringBuilder();
            users.ForEach(user => sb.Append($"{user}{Environment.NewLine}"));
            return sb.ToString();
        }
    }
}

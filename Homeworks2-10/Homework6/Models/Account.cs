﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework6.Models
{
    public class Account
    {
        public int Id { get; set; }
        public DateTimeOffset OpeningDate { get; set; }
        public decimal AccountBalance { get; set; }
        public int UserId { get; set; }

        public override string ToString() =>
            $"Account data - Id: {Id}, OpeningDate: {OpeningDate}, AccountBalance: {AccountBalance}, UserId: {UserId}";
    }
}

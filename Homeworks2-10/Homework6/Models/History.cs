﻿using System;

namespace Homework6.Models
{
    public class History
    {
        public int Id { get; set; }
        public DateTimeOffset OperationDate { get; set; }
        public AccountOperationType OperationType { get; set; }
        public decimal OperationAmount { get; set; }
        public int AccountId { get; set; }

        public override string ToString() => 
            $"History Operation data - Id: {Id}, OperationDate: {OperationDate}, OperationType: {OperationType}, OperationAmount: {OperationAmount}, AccountId: {AccountId}";
    }
}

﻿using System;

namespace Homework6.Models
{
    public class User
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string Patronymic { get; set; }
        public string Phone { get; set; }
        public string Passport { get; set; }
        public DateTimeOffset RegistrationDate { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }

        public override string ToString() => 
            $"User data - Id: {Id}, Name: {Name}, LastName: {LastName}, Patronymic: {Patronymic}, Phone: {Phone}, Passport: {Passport}, RegistrationDate: {RegistrationDate}, Login: {Login}";
    }
}

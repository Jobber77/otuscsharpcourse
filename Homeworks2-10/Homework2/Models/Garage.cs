﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace Homework2.Models
{
    /// <summary>
    /// This class represents simple Garage. Contains cars and has name.
    /// </summary>
    public class Garage
    {
        private readonly List<Car> _cars = new List<Car>();
        public string GarageName { get;}

        public Garage()
        {
            GarageName = "No Name";
        }

        public Garage(IEnumerable<Car> cars) : this()
        {
            _cars.AddRange(cars);
        }

        public Garage(IEnumerable<Car> cars, string garageName) : this(cars)
        {
            GarageName = garageName;
        }

        /// <summary>
        /// Add single car to Garage.
        /// </summary>
        /// <param name="car"></param>
        public void AddCar(Car car)
        {
            if (car != null) 
                _cars.Add(car);
        }

        /// <summary>
        /// Adds list of cars to Garage
        /// </summary>
        /// <param name="cars"></param>
        public void AddManyCars(IEnumerable<Car> cars) =>_cars.AddRange(cars);

        /// <summary>
        /// Gets single car, based on model name
        /// </summary>
        /// <param name="model"></param>
        /// <returns>First found car with supplied model</returns>
        public Car GetCar(string model) => _cars.FirstOrDefault(car => string.Equals(car.Model, model, StringComparison.InvariantCultureIgnoreCase));

        /// <summary>
        /// Remove firs found identical car.
        /// </summary>
        /// <param name="car"></param>
        public void RemoveCar(Car car) => _cars.Remove(car);

        /// <summary>
        /// Removes one car from Garage per each supplied in list car.
        /// </summary>
        /// <param name="cars"></param>
        public void RemoveManyCars(IEnumerable<Car> cars)
        {
            foreach (var car in cars)
            {
                _cars.Remove(car);
            }
        }

        /// <summary>
        /// Returns all cars in Garage.
        /// </summary>
        /// <returns>Returns all cars in Garage.</returns>
        public IEnumerable<Car> GetAllCars() => _cars.ToArray();

        /// <summary>
        /// Serves for addition of the cars from to Garages.
        /// </summary>
        /// <param name="garageOne"></param>
        /// <param name="garageTwo"></param>
        /// <returns>first garage from two</returns>
        public static Garage operator +(Garage garageOne, Garage garageTwo)
        {
            garageOne.AddManyCars(garageTwo.GetAllCars());
            return garageOne;
        }

        /// <summary>
        /// Add single car to Garage.
        /// </summary>
        /// <param name="garageOne"></param>
        /// <param name="garageTwo"></param>
        /// <returns>Garage</returns>
        public static Garage operator +(Garage garage, Car car)
        {
            garage.AddCar(car);
            return garage;
        }

        /// <summary>
        /// Delete cars from one garage from another.
        /// </summary>
        /// <param name="garageOne"></param>
        /// <param name="garageTwo"></param>
        /// <returns>Garage</returns>
        public static Garage operator -(Garage garageOne, Garage garageTwo)
        {
            garageOne.RemoveManyCars(garageTwo.GetAllCars());
            return garageOne;
        }

        /// <summary>
        /// Delete single car from Garage.
        /// </summary>
        /// <param name="garageOne"></param>
        /// <param name="garageTwo"></param>
        /// <returns>Garage</returns>
        public static Garage operator -(Garage garage, Car car)
        {
            garage.RemoveCar(car);
            return garage;
        }

        /// <summary>
        /// Serves to access cars in garage using index.
        /// </summary>
        /// <param name="index"></param>
        /// <returns>Car in supplied index.</returns>
        public Car this[int index]
        {
            get => _cars[index];
            set => _cars[index] = value;
        }

        /// <summary>
        /// Serves to access cars in garage using model name.
        /// </summary>
        /// <param name="index"></param>
        /// <returns>Car with supplied name</returns>
        public Car this[string model]
        {
            get => _cars.FirstOrDefault(car => string.Equals(car.Model, model, StringComparison.InvariantCultureIgnoreCase));
            set
            {
                var matchCarIndex = _cars.FindIndex(car => string.Equals(car.Model, model, StringComparison.InvariantCultureIgnoreCase));
                if(matchCarIndex != -1)
                    _cars[matchCarIndex] = value;
            }
        }
    }
}

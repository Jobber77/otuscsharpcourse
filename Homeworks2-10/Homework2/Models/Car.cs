﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework2.Models
{
    /// <summary>
    /// This class represents simple Car, contain only model.
    /// </summary>
    public class Car
    {
        public string Model { get; }

        public Car(string model)
        {
            Model = model;
        }

        /// <summary>
        /// Will be used for comparison of the cars
        /// </summary>
        /// <param name="carOne"></param>
        /// <param name="carTwo"></param>
        /// <returns>true if model identical</returns>
        public static bool operator ==(Car carOne, Car carTwo) => string.Equals(carOne?.Model, carTwo?.Model, StringComparison.InvariantCultureIgnoreCase);

        /// <summary>
        /// Will be used for comparison of the cars
        /// </summary>
        /// <param name="carOne"></param>
        /// <param name="carTwo"></param>
        /// <returns>true if models not identical</returns>
        public static bool operator !=(Car carOne, Car carTwo) => !string.Equals(carOne?.Model, carTwo?.Model, StringComparison.InvariantCultureIgnoreCase);

        /// <summary>
        /// Will be used for comparison of the cars
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        protected bool Equals(Car other)
        {
            return Model == other.Model;
        }

        /// <summary>
        /// Will be used for comparison of the cars
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((Car)obj);
        }

        /// <summary>
        /// Will be used for comparison of the cars
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public override int GetHashCode()
        {
            return (Model != null ? Model.GetHashCode() : 0);
        }

        /// <summary>
        /// Will be used in demo console app to print Model
        /// </summary>
        /// <param name="other"></param>
        /// <returns></returns>
        public override string ToString()
        {
            return Model;
        }
    }
}

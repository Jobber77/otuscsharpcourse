This project contains demo app for displaying Garage class capabilities.
Garage class contains list of Cars classes and methods / operators to work on it's content.
Garage class idea is the same as real Garage in real world - it contains Cars. You can add or remove some cars from Garage. You can move cars between Garages.

This homework took me about 1,5 hour together with work on Mentor's feedback.
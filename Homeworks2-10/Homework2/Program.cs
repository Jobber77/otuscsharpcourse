﻿using System;
using System.Collections.Generic;
using Homework2.Models;

namespace Homework2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("This is garage type demonstration app");
            var garageOne = new Garage(new List<Car>
            {
                new Car("Lada"),
                new Car("BMW")
            }, "Garage One");

            var garageTwo = new Garage(new List<Car>
            {
                new Car("Oka"),
                new Car("Audi")
            }, "Garage Two");

            Console.WriteLine("Initial Content");
            PrintGarageContent(garageOne);
            PrintGarageContent(garageTwo);

            Console.WriteLine("Test custom Operators");
            Console.WriteLine("Add one garage to another");
            garageOne = garageOne + garageTwo;
            PrintGarageContent(garageOne);

            Console.WriteLine("Substract one garage from another");
            garageOne = garageOne - garageTwo;
            PrintGarageContent(garageOne);

            Console.WriteLine("Test Indexator");
            Console.WriteLine($"Second Car in Garage");
            Console.WriteLine(garageOne[1]);

            Console.ReadLine();
        }

        private static void PrintGarageContent(Garage garage)
        {
            Console.WriteLine($"print cars of {garage.GarageName}");
            foreach (var car in garage.GetAllCars())
            {
                Console.WriteLine(car);  
            }
        }
    }
}

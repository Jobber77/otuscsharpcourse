﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework8.Models
{
    public class Foo
    {
        public int i1 { get; set; }
        public int i2 { get; set; }
        public int i3 { get; set; }
        public int i4 { get; set; }
        public int i5 { get; set; }
        public int i6 { get; set; }
        public static Foo GetFoo() => new Foo() { i1 = 1, i2 = 2, i3 = 3, i4 = 4, i5 = 5, i6 = 6 };
    }

}

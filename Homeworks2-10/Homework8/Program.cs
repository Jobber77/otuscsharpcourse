﻿using System;
using System.Diagnostics;
using System.IO;
using System.Text.Json;
using System.Text.Json.Serialization;
using Homework8.Models;
using Homework8.Services;

namespace Homework8
{
    class Program
    {
        static void Main(string[] args)
        {
            var foo = Foo.GetFoo();
            ICustomSerializer customSerializer = new CustomSerializer();
            Stopwatch sw = new Stopwatch();
            var result = String.Empty;

            Console.WriteLine("Custom serialization speed test:");

            sw.Start();
            for (int i = 0; i < 100_000; i++)
            {
               result = customSerializer.Serialize(foo);
            }
            sw.Stop();
            Console.WriteLine(result);
            Console.WriteLine($"Time spent to serialize 100 000 times using CustomSerializer: {sw.Elapsed}");
            
            Console.WriteLine("JsonSerializer serialization speed test:");
            sw.Reset();
            sw.Start();
            for (int i = 0; i < 100_000; i++)
            {
                result = JsonSerializer.Serialize(foo);
            }
            sw.Stop();
            Console.WriteLine(result);
            Console.WriteLine($"Time spent to serialize 100 000 times using JsonSerializer: {sw.Elapsed}");

            Console.WriteLine("Custom deserialization speed test:");
            Foo deSerialize = null;
            using (var stream = File.OpenText("Resources/CustomType.txt"))
            {
                sw.Reset();
                sw.Start();
                var text = stream.ReadToEnd();
                for (int i = 0; i < 100_000; i++)
                {
                    deSerialize = customSerializer.DeSerialize<Foo>(text);
                }
                sw.Stop();
            }
            Console.WriteLine($"Time spent to deserialize 100 000 times using CustomSerializer: {sw.Elapsed}");

            Console.WriteLine("Json deserialization speed test:");
            using (var stream = File.OpenText("Resources/JsonType.txt"))
            {
                sw.Reset();
                sw.Start();
                var text = stream.ReadToEnd();
                for (int i = 0; i < 100_000; i++)
                {
                    deSerialize = JsonSerializer.Deserialize<Foo>(text);
                }
                sw.Stop();
            }
            Console.WriteLine($"Time spent to deserialize 100 000 times using JsonSerializer: {sw.Elapsed}");

            Console.ReadKey();
        }
    }
}

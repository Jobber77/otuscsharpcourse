﻿using System.IO;

namespace Homework8.Services
{
    public interface ICustomSerializer
    {
        string Serialize(object obj);
        T DeSerialize<T>(string text);
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Homework8.Services
{
    public class CustomSerializer : ICustomSerializer
    {
        private BindingFlags DefaultFlags => BindingFlags.Public | BindingFlags.Instance;
        public char PropertiesDelimiter => ',';
        public char KeyValueDelimiter => ':';

        public string Serialize(object obj)
        {
            if (obj == null)
                return String.Empty;

            var type = obj.GetType();
            var fields = type.GetFields(DefaultFlags);
            var props = type.GetProperties(DefaultFlags);

            return SerializeProperties(obj, props);
        }

        public T DeSerialize<T>(string text)
        {
            if (string.IsNullOrEmpty(text))
                return default;

            var type = typeof(T);
            var props = type.GetProperties();
            var result = (T)Activator.CreateInstance(type);
            var stringProps = text.Split(PropertiesDelimiter);
            foreach (var propertyInfo in props)
            {
                var propType = propertyInfo.PropertyType;
                var propKey = $"{propertyInfo.Name}{KeyValueDelimiter}";
                var value = stringProps.FirstOrDefault(str => str.StartsWith(propKey))?.Substring(propKey.Length);
                if (string.IsNullOrEmpty(value))
                    continue;
                propertyInfo.SetValue(result, Convert.ChangeType(value, propType));
            }

            return result;
        }

        private string SerializeProperties(object obj, PropertyInfo[] props)
        {
            var stringProps = props.Select(prop => $"{prop.Name}{KeyValueDelimiter}{prop.GetValue(obj)}");
            return string.Join(PropertiesDelimiter, stringProps);
        }
    }
}

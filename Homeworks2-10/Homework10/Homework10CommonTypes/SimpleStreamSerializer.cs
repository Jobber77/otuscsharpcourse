﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Homework10CommonTypes
{
    public class SimpleStreamSerializer
    {
        public static void Serialize(Stream stream, NetworkMessage message)
        {
            try
            {
                var formatter = new BinaryFormatter();
                formatter.Serialize(stream, message);
            }
            catch (IOException)
            {

            }
        }

        public static NetworkMessage Deserialize(Stream stream)
        {
            try
            {
                var formatter = new BinaryFormatter();
                return (NetworkMessage)formatter.Deserialize(stream);
            }
            catch (SerializationException)
            {
                return NetworkMessage.GetErrorMessage(StatusCode.SerializationError);
            }
            catch (IOException)
            {
                return NetworkMessage.GetErrorMessage(StatusCode.Disconected);
            }
            catch (Exception)
            {
                return NetworkMessage.GetErrorMessage(StatusCode.UnknownError);
            }

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Homework10CommonTypes
{
    public enum StatusCode
    {
        Ok = 1,
        UnknownError = 2,
        SerializationError = 3,
        NetworkError = 4,
        Disconected = 5
    }
}

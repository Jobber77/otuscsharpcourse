﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace Homework10CommonTypes
{
    [Serializable]
    public class NetworkMessage : ISerializable
    {
        public StatusCode StatusCode { get; set; }
        public string UserName { get; set; }
        public string Text { get; set; }
        
        public NetworkMessage(StatusCode statusCode, string userName, string text)
        {
            StatusCode = statusCode;
            UserName = userName;
            Text = text;
        }

        public NetworkMessage(string userName, string text)
        {
            StatusCode = StatusCode.Ok;
            UserName = userName;
            Text = text;
        }

        public NetworkMessage(StatusCode statusCode, string text)
        {
            StatusCode = statusCode;
            Text = text;
        }

        public static NetworkMessage GetErrorMessage(StatusCode statusCode) => new NetworkMessage(statusCode, "Error occured");

        public override string ToString() => StatusCode == StatusCode.Ok 
                                                            ? $"{UserName}: {Text}" 
                                                            : $"We got an error. {StatusCode}: {Text}";

        public void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            info.AddValue("StatusCode", StatusCode, typeof(StatusCode));
            info.AddValue("UserName", UserName, typeof(string));
            info.AddValue("Text", Text, typeof(string));
        }

        private NetworkMessage(SerializationInfo info, StreamingContext context)
        {
            StatusCode = (StatusCode)info.GetValue("StatusCode", typeof(StatusCode));
            UserName = (string)info.GetValue("UserName", typeof(string));
            Text = (string)info.GetValue("Text", typeof(string));
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Threading.Tasks;
using Homework10CommonTypes;

namespace Homework10Server
{
    public class TcpServer
    {
        private readonly TcpListener _listener;
        private List<ClientContainer> _clients = new List<ClientContainer>();

        public TcpServer(TcpServerSettings settings)
        {
            if (settings == null)
                throw new ArgumentNullException(nameof(settings));

            _listener = IPAddress.TryParse(settings.IpAddress, out IPAddress address)
                ? new TcpListener(address, settings.Port)
                : new TcpListener(IPAddress.Any, settings.Port);
        }

        public async Task RunAsync(CancellationToken token)
        {
            try
            {
                _listener.Start();
                await StartListening(token);
            }
            catch (Exception e)
            {
                Console.WriteLine("An unhandled exception occured :(");
                Disconnect();
            }
            
        }

        private async Task StartListening(CancellationToken token)
        {
            Console.WriteLine("Server started. Awaiting clients.");
            while (!token.IsCancellationRequested)
            {
                TcpClient tcpClient = await _listener.AcceptTcpClientAsync();
                var client = new ClientContainer(tcpClient, this);
                _ = Task.Run(client.Process, token);
            }
            Disconnect();
        }

        internal void RegisterClient(ClientContainer client)
        {
            _clients.Add(client);
        }

        internal void RemoveClient(Guid id)
        {
            var client = _clients.FirstOrDefault(c => c.Id == id);
            if (client != null)
                _clients.Remove(client);
        }

        internal void Disconnect()
        {
            _listener.Stop();
            foreach (var client in _clients)
            {
                client.Dispose();
            }
            Console.WriteLine("Shutting down the server. Bye.");
            Environment.Exit(0);
        }

        protected internal void SendMessageToAllClients(NetworkMessage message, Guid id)
        {
            foreach (var client in _clients)
            {
                if (client.Id != id)
                    SimpleStreamSerializer.Serialize(client.Stream, message);
            }
        }
    }
}

﻿namespace Homework10Server
{
    public class TcpServerSettings
    {
        public string IpAddress { get; set; }
        public int Port { get; set; }
    }
}

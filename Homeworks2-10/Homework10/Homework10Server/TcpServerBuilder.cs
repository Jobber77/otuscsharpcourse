﻿using Microsoft.Extensions.Configuration;
using System.IO;
using System.Net;
using System.Net.Sockets;

namespace Homework10Server
{
    public class TcpServerBuilder
    {
        public TcpServer BuildDefaultServer()
        {
            var settings = new TcpServerSettings();
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");
            builder.Build().Bind("TcpServer", settings);

            return new TcpServer(settings);
        }
    }
}

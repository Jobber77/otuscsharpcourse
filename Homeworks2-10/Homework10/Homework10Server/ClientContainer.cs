﻿using System;
using System.Net.Sockets;
using Homework10CommonTypes;

namespace Homework10Server
{
    internal class ClientContainer : IDisposable
    {
        internal Guid Id { get; }
        internal NetworkStream Stream { get; }
        private readonly TcpClient _client;
        private readonly TcpServer _server;

        public ClientContainer(TcpClient tcpClient, TcpServer server)
        {
            Id = Guid.NewGuid();
            _client = tcpClient;
            _server = server;
            Stream = _client.GetStream();
            _server.RegisterClient(this);
        }

        public void Process()
        {
            var message = SimpleStreamSerializer.Deserialize(Stream);
            SendMessage(message);
            while (true)
            {
                message = SimpleStreamSerializer.Deserialize(Stream);
                SendMessage(message);
                if (message.StatusCode != StatusCode.Ok)
                {
                    _server.RemoveClient(Id);
                    Dispose();
                    break;
                }
            }
        }

        private void SendMessage(NetworkMessage message)
        {
            Console.WriteLine(message.ToString());
            _server.SendMessageToAllClients(message, Id);
        }

        public void Dispose()
        {
            Stream?.Close();
            _client?.Close();
        }
    }
}

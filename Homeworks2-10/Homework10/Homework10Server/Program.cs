﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Homework10Server
{
    class Program
    {
        static async Task Main(string[] args)
        {
            await new TcpServerBuilder().BuildDefaultServer().RunAsync(CancellationToken.None);
        }
    }
}

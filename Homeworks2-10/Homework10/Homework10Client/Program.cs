﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace Homework10Client
{
    class Program
    {
        static void Main(string[] args)
        {
            var client = new ConsoleTcpClientBuilder().BuildDefaultServer();
            _ = Task.Run(client.ReceiveMessage, CancellationToken.None);

            Console.WriteLine("Type your messages and hit Enter.");
            while (true)
            {
                client.SendMessage(Console.ReadLine());
            }
        }
    }
}

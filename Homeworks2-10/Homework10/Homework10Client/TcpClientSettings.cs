﻿namespace Homework10Client
{
    public class TcpClientSettings
    {
        public string ServerIpAddress { get; set; }
        public int ServerPort { get; set; }
        public string UserName { get; set; }
    }
}

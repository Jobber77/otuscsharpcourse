﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using Homework10CommonTypes;

namespace Homework10Client
{
    public class ConsoleTcpClient
    {
        private readonly NetworkStream _stream;
        private readonly string _userName;
        private readonly TcpClient _client;

        public ConsoleTcpClient(TcpClientSettings settings)
        {
            if (settings == null)
                throw new ArgumentNullException(nameof(settings));

            _userName = settings.UserName;
            _client = new TcpClient();
            _client.Connect(settings.ServerIpAddress, settings.ServerPort);
            _stream = _client.GetStream();
            SimpleStreamSerializer.Serialize(_stream, new NetworkMessage(_userName, "Enter Chat"));
        }

        public void SendMessage(string messageText)
        {
            SimpleStreamSerializer.Serialize(_stream, new NetworkMessage(_userName, messageText));
        }

        public void ReceiveMessage()
        {
            while (true)
            {
                var message = SimpleStreamSerializer.Deserialize(_stream);
                Console.WriteLine(message.ToString());
            }
        }
    }
}

﻿using System.IO;
using Microsoft.Extensions.Configuration;

namespace Homework10Client
{
    public class ConsoleTcpClientBuilder
    {
        public ConsoleTcpClient BuildDefaultServer()
        {
            var settings = new TcpClientSettings();
            var builder = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json");
            builder.Build().Bind("TcpClient", settings);

            return new ConsoleTcpClient(settings);
        }
    }
}

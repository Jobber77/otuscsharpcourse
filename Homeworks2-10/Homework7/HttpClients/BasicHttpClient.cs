﻿using Microsoft.Extensions.Logging;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Homework7.HttpClients
{
    public class BasicHttpClient : IHttpClient
    {
        private ILogger<BasicHttpClient> _logger;
        private HttpClient _httpClient;

        public BasicHttpClient(ILogger<BasicHttpClient> logger)
        {
            _logger = logger;
            _httpClient = new HttpClient();
        }

        public async Task<string> GetPageContentAsync(string url)
        {
            try
            {
                var result = await _httpClient.GetAsync(url);
                return await result.Content.ReadAsStringAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError("Error fetching web page content", ex);
                return null;
            }
        }

        public void Dispose()
        {
            _httpClient?.Dispose();
        }
    }
}

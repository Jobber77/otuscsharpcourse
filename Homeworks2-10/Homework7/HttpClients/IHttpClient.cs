﻿using System;
using System.Threading.Tasks;

namespace Homework7.HttpClients
{
    public interface IHttpClient : IDisposable
    {
        Task<string> GetPageContentAsync(string url);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Homework7.Parsers
{
    public interface IWebPageParser : IDisposable
    {
        Task<IEnumerable<string>> GetWebPageUrlsAsync(string webPageLink);
    }
}

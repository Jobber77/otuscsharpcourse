﻿using Homework7.HttpClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Homework7.Parsers
{
    public class WebPageParser : IWebPageParser
    {
        private IHttpClient _httpClient;
        private const string _regexpForUrls = 
            @"https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.?[a-zA-Z0-9()]{1,6}?[\/?a-zA-Z0-9@:%._\+~#=]*";

        public WebPageParser(IHttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<IEnumerable<string>> GetWebPageUrlsAsync(string webPageLink)
        {
            var pageContent = await _httpClient.GetPageContentAsync(webPageLink);

            if (pageContent == null)
                return Enumerable.Empty<string>();

            var result = Regex.Matches(pageContent, _regexpForUrls, RegexOptions.IgnoreCase);
            return result.Select(match => match.Value);
        }

        public void Dispose()
        {
            _httpClient?.Dispose();
        }
    }
}

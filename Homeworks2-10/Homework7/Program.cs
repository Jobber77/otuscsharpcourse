﻿using Homework7.HttpClients;
using Homework7.Parsers;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Homework7
{
    class Program
    {
        static async Task Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            using (var serviceProvider = serviceCollection.BuildServiceProvider())
            using (var scope = serviceProvider.CreateScope())
            {
                Console.WriteLine("Введите url страницы для поиска ссылок.");
                var url = Console.ReadLine();
                var parser = scope.ServiceProvider.GetService<IWebPageParser>();
                var urls = await parser.GetWebPageUrlsAsync(url);
                var text = urls.Any()
                        ? "Найденные ссылки:"
                        : "Ссылок не найдено. Указанный url некоректен или страница не содержит ссылок.";
                Console.WriteLine(text);
                foreach (var link in urls)
                {
                    Console.WriteLine(link);
                }
                Console.ReadLine();   
            }
        }

        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddLogging(configure => configure.AddConsole());
            services.AddTransient<IHttpClient, BasicHttpClient>();
            services.AddTransient<IWebPageParser, WebPageParser>();
        }
    }
}
